from flask import Flask, request, make_response, jsonify, abort
from pymongo import MongoClient
from bson.json_util import dumps
from bot.botthread import Bot
import botconfig
import bot.botprocessor

app = Flask(__name__)


client = MongoClient()
client = MongoClient(botconfig.db_host, botconfig.db_port)
user_db = client.user


@app.route('/bot/api/v1.0/user/data', methods=['PUT'])
def put_user_data():
    if not request.json:
        abort(400)

    auth_data = user_db.auth_data
    data = {
        'token': request.json['token'],
        'login': request.json['login'],
        'chatid': request.json['chatid']
    }
    auth_data.delete_many({'login':request.json['login']})
    auth_data.insert_one(data)
    return jsonify({'done': 'true'}), 201


@app.route('/bot/api/v1.0/user/data/<login>', methods=['GET'])
def get_user_data(login):
    auth_data = user_db.auth_data
    data = auth_data.find_one({"login" : login})
    if data is None:
        return make_response(jsonify({'error': 'Not found'}), 200)
    return dumps(data), 200


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


if __name__ == "__main__":

    app.run(host=botconfig.http_host, )