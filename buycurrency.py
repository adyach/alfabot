__author__ = 'andrey'
# -*- coding: utf-8 -*-
import urllib2
import urllib
from pymongo import MongoClient
import botconfig
import ssl
import StringIO
import gzip
import getaccounts
import logging

client = MongoClient()
client = MongoClient(botconfig.db_host, botconfig.db_port)
user_db = client.user

def buy_currency_step_1(chat_id, accounts, amount, currency):
    auth_data = user_db.auth_data
    projection = {'chatid' : chat_id}
    user = auth_data.find_one(projection)
    url = 'https://testjmb.alfabank.ru/FT7JMB3/ControllerServlet;jsessionid=' + acs[0] #+ user['token'])

    f = open('data.txt', 'r')
    binary_data = f.read()
    binary_data = binary_data.replace('acc1', str(accounts[1]))
    binary_data = binary_data.replace('acc2', str(accounts[2]))
    f.close()

    # data = "<?xml version='1.0' ?><d o=\"313\"><f1>" + str(accounts[1]) + "</f1><f2>" + str(accounts[2]) + "</f2><f3>100</f3><f4>RUR</f4></d>"

    headers = {
        'Accept-Encoding': 'gzip, deflate',
        'Content-Type': 'application/octet-stream',
        'Accept-Language': 'ru',
        'Accept': '*/*',
        'Connection': 'keep-alive',
        'User-Agent': 'AlfaMobile_iPhone/6416',
        'Host': 'testjmb.alfabank.ru',
        'Proxy-Connection': 'keep-alive'
        }

    req = urllib2.Request(url, binary_data, headers)

    try:
        resp = urllib2.urlopen(req)
        if resp.info().get('Content-Encoding') == 'gzip':
            buf = StringIO.StringIO(resp.read())
            f = gzip.GzipFile(fileobj=buf)
            content = f.read()
            print content
    except urllib2.HTTPError, err:
        response_body = err.read()

    return None


def buy_currency_step_2(chat_id, accounts, accs, id):
    auth_data = user_db.auth_data
    projection = {'chatid' : chat_id}
    user = auth_data.find_one(projection)
    url = 'https://testjmb.alfabank.ru/FT7JMB3/ControllerServlet;jsessionid=' + acs[0] #+ user['token']
    data = "l<?xml version='1.0' ?><d o=\""+id+ "\"><f1>" + str(accounts[1]) + "</f1><f2>" + str(accounts[2]) + "</f2></d>"
    headers = {
        'Accept-Encoding': 'gzip, deflate',
        'Content-Type': 'application/octet-stream',
        'Accept-Language': 'ru',
        'Accept': '*/*',
        'Connection': 'keep-alive',
        'User-Agent': 'AlfaMobile_iPhone/6416',
        'Host': 'testjmb.alfabank.ru'
        }


    req = urllib2.Request(url, data, headers)

    try:
        resp = urllib2.urlopen(req)
        response_body = resp.read()
    except urllib2.HTTPError, err:
        response_body = err.read()

    return response_body


if __name__ == "__main__":

    acs = getaccounts.get_accounts_by_currency(None, 'USD')
    a = buy_currency_step_1(None, acs, 100, 'USD')
    print a
