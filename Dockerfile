FROM ubuntu
MAINTAINER Andrey Dyachkov

RUN echo "deb http://archive.ubuntu.com/ubuntu/ $(lsb_release -sc) main universe" >> /etc/apt/sources.list
RUN apt-get update
RUN apt-get install -y git
RUN apt-get install -y python python-dev python-distribute python-pip
RUN git clone https://andreythegreatest@bitbucket.org/andreythegreatest/alfabot.git
RUN pip install -r /alfabot/requirements.txt

EXPOSE 80
EXPOSE 5000
WORKDIR /alfabot
CMD python botserver.py
CMD python botthread.py