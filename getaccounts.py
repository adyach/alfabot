# -*- coding: utf-8 -*-
import urllib2
from pymongo import MongoClient
import botconfig
import logging
import ssl
import StringIO
import gzip
import json

client = MongoClient()
client = MongoClient(botconfig.db_host, botconfig.db_port)
user_db = client.user

def get_accounts_by_currency(chat_id, currency):
    auth_data = user_db.auth_data
    projection = {'chatid' : chat_id}
    user = auth_data.find_one(projection)
    session = get_session()
    url = 'https://testjmb.alfabank.ru/FT7JMB3/gate;jsessionid=' + session #+ user['token']
    data = "{\"operationId\":\"Budget:GetAccounts\",\"parameters\":{\"operation\":\"mainPage\"}}"
    headers = {
        'Accept-Encoding': 'gzip, deflate',
        'Content-Type': 'application/octet-stream',
        'Accept-Language': 'ru',
        'jmb-protocol-version': '1.0',
        'Accept': '*/*',
        'Connection': 'keep-alive',
        'User-Agent': 'AlfaMobile_iPhone/6416',
        'jmb-protocol-service': 'Budget',
        'Host': 'testjmb.alfabank.ru'
        }

    account_number_from = None
    account_number_to = None
    try:
        request = urllib2.Request(url, data, headers)
        response = urllib2.urlopen(request)
        if response.info().get('Content-Encoding') == 'gzip':
            buf = StringIO.StringIO(response.read())
            f = gzip.GzipFile(fileobj=buf)
            content = f.read()
            data = json.loads(content)
            for account in data['fields'][1]['value']:
                if account['description'] == u'Текущий счёт' and account['currency'] == 'RUR' and float(account['amount']) > 100:
                    account_number_from = account['number']
                if account['currency'] == currency:
                    account_number_to = account['number']
            print account_number_from
        else:
            content = request.read()
    except:
        pass
    accs = [session, account_number_from, account_number_to]
    print accs
    return accs


def get_session():
    ssl._create_default_https_context = ssl._create_unverified_context

    url = 'https://testjmb.alfabank.ru/FT7JMB3/gate'

    headers = {
        'Accept-Encoding': 'gzip, deflate',
        'Content-Type': 'application/octet-stream',
        'Accept-Language': 'ru',
        'jmb-protocol-version': '1.0',
        'Accept': '*/*',
        'Connection': 'keep-alive',
        'User-Agent': 'AlfaMobile_iPhone/6416',
        'jmb-protocol-service': 'Authorization',
        'Host': 'testjmb.alfabank.ru'

        }

    body = "{\"operationId\":\"Authorization:Login\",\"parameters\":{\"locale\":\"ru\",\"login\":\"1890999\",\"password\":\"111222\",\"appVersion\":\"6.6.0\",\"operationSystem\":\"iOS\",\"loginType\":\"password\"}}"

    req = urllib2.Request(url, body, headers)

    try:
        resp = urllib2.urlopen(req)
        response_body = resp.read()
    except urllib2.HTTPError, err:
        response_body = err.read()

    a = resp.info().getheader("Set-Cookie").split('=')[1].replace("; path", "")
    logging.log(logging.WARN,"a=%s", a)

    return a



if __name__ == "__main__":

    a = get_accounts_by_currency(None, 'USD')
    logging.log(logging.WARN,"a=%s", a)
