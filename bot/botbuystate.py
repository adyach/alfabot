# -*- coding: utf-8 -*-

__author__ = 'andrey'
from pymongo import MongoClient

import botcommand
import botconfig
import currency
import bot.botthread

client = MongoClient()
client = MongoClient(botconfig.db_host, botconfig.db_port)
user_db = client.user


class Context:
    update = None
    bot = None
    data = None
    state = None

    def set_state(self, state):
        self.state = state

    def get_state(self):
        return self.state

    def apply(self, input_data):
        if self.state is not None:
            return self.state.apply(self, input_data)
        else:
            return None

    def set_data(self, data):
        self.data = data

    def get_data(self):
        return self.data

    def set_update(self, update):
        self.update = update

    def set_bot(self, bot):
        self.bot = bot

    def response(self):
        return self.data.response()


class BuyData:
    rate = None
    currency = None
    sum = None

    def response(self):
        return str(self.rate) + "\n" + str(self.currency) + "\n" + str(self.sum)


class AlertData:
    howoften = None


class BuyState:
    def apply(self, context, input_data):
        context.data = BuyData()
        context.set_state(BuyCurrencyState())
        # save to DB
        return None


class BuyCurrencyState:
    def apply(self, context, input_data):
        if input_data == 'EUR' or input_data == 'USD':
            context.data.rate = currency.get_rate_by_currency(input_data)
            context.data.currency = input_data
            context.set_state(BuySumState())
            # save to DB
            return botcommand.Command("response").send_message(context.bot, context.update, "Введите сумму")


class BuySumState:
    def apply(self, context, input_data):
        if input_data.isdigit():
            context.data.sum = input_data
            if is_auth(context.update.message.chat_id):
                context.set_state(BuyConfirmState())
                botcommand.Command("response").send_message(context.bot, context.update, context.response())
                return botcommand.Command("response").send_keyboard(context.bot, context.update, [["Купить"]],
                                                                    "Покупаем?", True)
            else:
                context.set_state(AuthState())
                return botcommand.AuthorizeCommand().fire_command(context.bot, context.update)


class AuthState:
    def apply(self, context, input_data):
        if is_auth(context.update.message.chat_id):
            context.set_state(BuyConfirmState)
            botcommand.Command("response").send_message(context.bot, context.update, context.response())
            return botcommand.Command("response").send_keyboard(context.bot, context.update, [["Купить"]], "Покупаем?",
                                                                True)
        botcommand.Command("response").send_keyboard(context.bot, context.update, [["Купить"]], "Покупаем?", False)
        return botcommand.AuthorizeCommand().fire_command(context.bot, context.update)


def is_auth(chatid):
    auth_data = user_db.auth_data
    data = auth_data.find_one({"chatid": chatid})
    if data is None:
        return False
    else:
        return True


class BuyConfirmState:
    def apply(self, context, input_data):
        context.set_state(None)
        return botcommand.Command("response").send_message(context.bot, context.update, "Готово")


class BuyConfirmState:
    def apply(self, context, input_data):
        context.set_state(None)
        return botcommand.Command("response").send_message(context.bot, context.update, "Готово")


class SetupAlertState:
    def apply(self, context, input_data):
        context.set_state(SetupAlertsHowOften())
        return botcommand.Command("response").send_message(context.bot, context.update, "Готово")


class SetupAlertsHowOften:
    def apply(self, context, input_data):
        context.set_state(None)
        if input_data == u'Раз в 10 секунд':
            bot.botthread.array_15_sec.append(context.update.message.chat_id);
            print bot.botthread.array_15_sec
        elif input_data == u'Отслеживать изменения':
            context.set_state(SetupAlertCurrency());
            return botcommand.Command("response") \
                .send_keyboard(context.bot, context.update, [["EUR", "USD"]], "Выберите валюту?", True)


class SetupAlertCurrency:
    def apply(self, context, input_data):
        context.set_state(None)
        message = None
        if input_data == u'EUR':
            context.set_state(SetupAlertAmount("EUR"))
            message = u'Укажите значение курса'
        elif input_data == 'USD':
            context.set_state(SetupAlertAmount("RUR"))
            message = u'Укажите значение курса'
        else:
            message = u'Данная валюта не поддерживается'
        return botcommand.Command("response").send_message(context.bot, context.update, message)


def is_float(value):
    if value.isdigit():
        return True
    try:
        float(value)
        return True
    except Exception:
        return False


class SetupAlertAmount:
    currency = None

    def __init__(self, currency):
        self.currency = currency

    def apply(self, context, input_data):
        context.set_state(None)
        if not is_float(input_data):
            context.set_state(SetupAlertAmount(self.currency))
            return botcommand.Command("response").send_message(context.bot, context.update, "Введите число")
        rate = currency.get_rate_by_currency_as_float(self.currency)
        check_rate = bot.botthread.CheckRate()
        check_rate.chat_id = context.update.message.chat_id
        if rate > input_data:
            check_rate.equalType = "less"
        elif rate < input_data:
            check_rate.equalType = "more"
        else:
            botcommand.Command("response") \
                .send_message(context.bot, context.update, "Текущий курс совпадает с заданым")
            return;
        check_rate.currency = self.currency
        check_rate.rate = rate
        checkRateList = sorted(set(bot.botthread.array_15_sec_check_rate))
        for item in checkRateList:
            if item.chat_id == check_rate.chat_id:
                item.equalType = check_rate.equalType
                item.currency = check_rate.currency
                item.rate = rate
                break
        else:
            bot.botthread.array_15_sec_check_rate.append(check_rate)
        return botcommand.Command("response") \
            .send_message(context.bot, context.update, "Спасибо. При изменении курса с текущего "
                          + str(rate) + " на " + str(input_data) + " вам придет оповещение.")
