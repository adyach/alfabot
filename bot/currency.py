# -*- coding: utf-8 -*-

import urllib2
from xml.dom.minidom import parseString


class Currency:
    title = ""
    buy = ""
    sell = ""

    def __str__(self):
        return self.title + " " + self.sell


def get_rate():
    xmlurl = 'https://abm.alfabank.ru/ALBOBackEnd/ProdDock?login=alfabot1&pair=USD/RUR&pair=EUR/RUR&method=GetAlfaBankRates'

    # ssl._create_default_https_context = ssl._create_unverified_context
    req = urllib2.Request(xmlurl)

    resp = urllib2.urlopen(req)
    content = resp.read()

    dom = parseString(content)

    currencies = []
    for node in dom.getElementsByTagName('rate'):
        c = Currency()
        c.title = node.getElementsByTagName("pair")[0].firstChild.data
        c.buy = node.getElementsByTagName("priceBid")[0].firstChild.data
        c.sell = node.getElementsByTagName("priceOffer")[0].firstChild.data

        c.title = c.title.replace("/RUR", "")
        currencies.append(c)
        print(c)

    x = '\n'.join([str(x) for x in currencies])
    return x


def get_rate_by_currency(currency):
    xmlurl = 'https://abm.alfabank.ru/ALBOBackEnd/ProdDock?login=alfabot1&pair=USD/RUR&pair=EUR/RUR&method=GetAlfaBankRates'

    # ssl._create_default_https_context = ssl._create_unverified_context
    req = urllib2.Request(xmlurl)

    resp = urllib2.urlopen(req)
    content = resp.read()

    dom = parseString(content)

    currencies = []
    for node in dom.getElementsByTagName('rate'):
        c = Currency()
        c.title = node.getElementsByTagName("pair")[0].firstChild.data
        c.buy = node.getElementsByTagName("priceBid")[0].firstChild.data
        c.sell = node.getElementsByTagName("priceOffer")[0].firstChild.data

        c.title = c.title.replace("/RUR", "")
        if currency == c.title:
            currencies.append(c)
            print(c)

    return [str(x) for x in currencies]


def get_rate_by_currency_as_float(currency):
    try:
        current_rate = get_rate_by_currency(currency)
        return current_rate[0][current_rate[0].find(' ') + 1:]
    except Exception:
        return 0
