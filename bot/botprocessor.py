__author__ = 'andrey'

import botcommand
import botbuystate
import logging


contexts = {}


def process_message(bot, update):
    commands = [botcommand.DefaultCommand(),
                botcommand.AuthorizeCommand(),
                botcommand.AccountsCommand(),
                botcommand.StartCommand(),
                botcommand.HelpCommand(),
                botcommand.CancelCommand(),
                botcommand.ShowRatesCommand(),
                botcommand.BuyCurrencyCommand(),
                botcommand.SetupAlerts()]

    state_context = None
    chatid = str(update.message.chat_id)
    if chatid in contexts.keys():
        state_context = contexts[chatid]

    if state_context is None:
        state_context = botbuystate.Context()
        contexts[chatid] = state_context

    state_context.set_bot(bot)
    state_context.set_update(update)

    command_name = update.message.text
    logging.log(logging.WARN,"state=%s", state_context.get_state())
    logging.log(logging.WARN,"command_name=%s", command_name)

    # process commands and set state
    # if command was not found then try to check state
    for cmd in commands:
        result = cmd.fire(command_name, bot, update)
        if result is not None:
            state_context.set_state(cmd.get_state())
            result_state = state_context.apply(command_name)
            if result_state is not None:
                return result_state
            return result

    result = state_context.apply(command_name)
    if result is None:
        return update.update_id + 1
        #return botcommand.DefaultCommand().fire_command(bot, update)

    return result