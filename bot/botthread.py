# -*- coding: utf-8 -*-
import logging
from time import sleep
import threading
import sched
import time

import telegram

import botprocessor
import currency


# from twisted.internet import task
# from twisted.internet import reactor

try:
    from urllib.error import URLError
except ImportError:
    from urllib2 import URLError  # python 2

array_15_sec = []
array_15_sec_check_rate = []


class Bot:
    bot = None

    def check15SecRate(self, check_rate):
        rate = currency.get_rate_by_currency_as_float(check_rate.currency)
        if check_rate.equalType == u'more':
            if check_rate.rate < rate:
                text = u'Курс изменился с ' + str(check_rate.rate) + u' на ' + str(rate)
                self.bot.sendMessage(chat_id=check_rate.chat_id, text=text)
        elif check_rate.equalType == u'less':
            if check_rate.rate > rate:
                text = u'Курс изменился с ' + str(check_rate.rate) + u' на ' + str(rate)
                self.bot.sendMessage(chat_id=check_rate.chat_id, text=text)

    def serve(self):
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        thread = threading.Thread(target=self.top_loop, args=())
        thread.daemon = True
        thread.start()
        self.start_updating_15_sec()

    def top_loop(self):

        logging.log(logging.WARN, "bot in top_loop")

        update_id = None
        self.bot = telegram.Bot('175861138:AAEO8frMP0q9eeGXZNp1Le33bsRY4A4KF14')

        while True:
            try:
                update_id = server_inner(self.bot, update_id)
            except telegram.TelegramError as e:
                # These are network problems with Telegram.
                if e.message in ("Bad Gateway", "Timed out"):
                    sleep(1)
                else:
                    raise e
            except URLError as e:
                # These are network problems on our end.
                sleep(1)

    def start_updating_15_sec(self):
        s = sched.scheduler(time.time, time.sleep)

        def do_something(sc):
            sc.enter(15, 1, do_something, (sc,))
            rates = currency.get_rate()
            myList = sorted(set(array_15_sec))
            for chat_id in myList:
                self.bot.sendMessage(chat_id=chat_id, text=rates)
            checkRateList = sorted(set(array_15_sec_check_rate))
            for checkRate in checkRateList:
                self.check15SecRate(checkRate)

        s.enter(15, 1, do_something, (s,))
        s.run()


class CheckRate:
    chat_id = None
    equalType = None
    currency = None
    rate = None


def server_inner(bot, update_id):
    # Request updates after the last update_id
    for update in bot.getUpdates(offset=update_id, timeout=10):
        logging.log(logging.WARN, "bot update=%s", update)
        update_id = botprocessor.process_message(bot, update)

    return update_id


if __name__ == "__main__":

    bot = Bot()
    bot.serve()