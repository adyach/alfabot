# -*- coding: utf-8 -*-
import urllib

from pymongo import MongoClient
import telegram

import botconfig
import botbuystate
import currency
import getaccounts

client = MongoClient()
client = MongoClient(botconfig.db_host, botconfig.db_port)
user_db = client.user
state_context = botbuystate.Context()

class Command:

    name = None

    def __init__(self, name):
        self.name = name

    def fire(self, name, bot, update):
        if self.name == name:
            return self.fire_command(bot, update)

    def fire_command(self, bot, update):
        raise NotImplementedError

    def send_message(self, bot, update, text):
        chat_id = update.message.chat_id
        update_id = update.update_id + 1
        bot.sendMessage(chat_id=chat_id, text=text)
        return update_id

    def send_keyboard(self, bot, update, custom_keyboard, text, one_time):
        chat_id = update.message.chat_id
        update_id = update.update_id + 1
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, one_time_keyboard=one_time)
        bot.sendMessage(chat_id=chat_id, text=text, reply_markup=reply_markup)
        return update_id

    def get_state(self):
        return None


class DefaultCommand(Command):

    def __init__(self):
        Command.__init__(self, "")

    def fire_command(self, bot, update):
        return self.send_message(bot, update, "Check your command syntax")


class AuthorizeCommand(Command):

    def __init__(self):
        Command.__init__(self, "/authorize")

    def fire_command(self, bot, update):
        ios_url = "Для авторизации бота в IOS: alfabank://auth?" + urllib.quote_plus(str.format("endpoint={}&chatid={}", "107.170.132.24:5000/bot/api/v1.0/user/data", str(update.message.chat_id)))
        self.send_message(bot, update, ios_url)
        android_url = str.format("Для авторизации бота в ANDROID: http://alfabank.telegram/auth?endpoint={}&chatid={}", "http://107.170.132.24:5000/bot/api/v1.0/user/data", str(update.message.chat_id))
        return self.send_message(bot, update, android_url)


class AccountsCommand(Command):

    def __init__(self):
        Command.__init__(self, "/accounts")

    def fire_command(self, bot, update):
        return self.send_message(bot, update, getaccounts.fire_command(update.message.chat_id))


class StartCommand(Command):

    def __init__(self):
        Command.__init__(self, "/start")

    def fire_command(self, bot, update):
        return self.send_keyboard(bot, update, [["Показать курсы"],["Купить валюту"],["Настроить оповещения"]], "привет", False)


class HelpCommand(Command):

    def __init__(self):
        Command.__init__(self, "/help")

    def fire_command(self, bot, update):
        return self.send_message(bot, update, "Help!")


class ShowRatesCommand(Command):

    def __init__(self):
        Command.__init__(self, u"Показать курсы")

    def fire_command(self, bot, update):
        # TODO request for currencies
        return self.send_keyboard(bot, update, [["Показать курсы"],["Купить валюту"],["Настроить оповещения"]], currency.get_rate(), False)


class BuyCurrencyCommand(Command):

    def __init__(self):
        Command.__init__(self, u"Купить валюту")

    def fire_command(self, bot, update):
        return self.send_keyboard(bot, update, [["EUR", "USD"]], currency.get_rate(), True)

    def get_state(self):
        return botbuystate.BuyState()


class SetupAlerts(Command):

    def __init__(self):
        Command.__init__(self, u"Настроить оповещения")

    def fire_command(self, bot, update):
        return self.send_keyboard(bot, update, [["Все изменения"], ["Раз в 10 секунд"],["Отслеживать изменения"]], "Как часто?", True)

    def get_state(self):
        return botbuystate.SetupAlertState()


class CancelCommand(Command):

    def __init__(self):
        Command.__init__(self, u"Oтмена")

    def fire_command(self, bot, update):
        return StartCommand().fire_command(self, bot, update)